##########
Discussion
##########

Packaging
=========
Creating a package using :py:mod:`distutils` was virtually painless, one the
preliminary work of organising the module had been performed. The only hurdle 
was implementing the ``MANIFEST.in`` file, and it was a very small hurdle 
indeed.


Documenting
===========
Documenting in Sphinx is done using an implementation of the *reStructuredText*
(*rst*) framework [rst]_. I found this not to be as convenient or flexible as LaTeX,
but this is more than balanced by the automatic documentation features, and
the powerful and flexible build options that Sphinx provides. I may not start 
using rst instead of LaTeX, but I will certainly put up with it for documentation
purposes.


Tests?
======
Aside from using :py:mod:`unittest` [unittest]_, Sphinx also has an extension 
for including tests in the documentation [doctest]_. Unfortunately, time did 
not permit trying either of these out.


Conclusions and observations
============================
By starting out a project with :py:mod:`distutils` and automatic documentation
using e.g. Sphinx, I believe better coding practice will come naturally. This
is certainly also true, if one has in mind from the beginning to use the 
automatic Sphinx test functionality, which is an avenue I hope to explore in
the future.

