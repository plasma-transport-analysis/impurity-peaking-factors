.. Impurity Peaking Factors documentation master file, created by
   sphinx-quickstart on Thu Mar  1 12:05:41 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Impurity Peaking Factors' documentation!
====================================================

Abstract
--------
Welcome the documentation for the *Impurity Peaking Factors* module!

This document serves the dual purposes of being both a (brief) introduction to
the module in question, including documentation of the sub-modules and their
constituting functions, and as a report for the project in the course 
*"Python and High Performance Computing"* given by :math:`C^3SE` at 
*Chalmers University of Technology* in the spring term of 2012 [c3se]_.

If you are only interested in installing and using the software, you can safely
jump to :doc:`implementation` or :doc:`Appendix A <modules>`.


Contents:
---------

.. toctree::
    :maxdepth: 3

    introduction
    solution
    implementation
    discussion
    
    Appendix A: The packaged module <modules>

    plan

    references

Indices
=======

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

