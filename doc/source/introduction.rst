###############
Introduction...
###############

... to the package
===================
The Impurity Peaking Factors package is a collection of modules meant for
analysing data from GENE [GENE]_ *nrg* data files, that is data files with
comprehensible physical data in the form of scalars, usually in the form
of time series data.

The package contains functions specifically meant for calculating the 
uncertainty in such time series, and for calculating *peaking factors* 
from flux data.

The flux for a particle species with charge :math:`Z` can be locally 
described as consisting of a diffusive and a convective part according to:

.. _GammaEq:
.. math:: \Gamma_Z = -D_Z \nabla n_Z + n_Z V_Z \Leftrightarrow R \Gamma/n_Z = -D_Z R/L_{n_Z} + R V_Z.

From this, the peaking factor :math:`PF` can be defined through the ratio of 
the convective velocity and the diffusivity, obtained at the point where
:math:`\Gamma = 0`:

.. _PFEq:
.. math :: PF = -\frac{R V_Z}{D_Z}.

:math:`PF` can therefore be interpreted as the "gradient of zero flux", and
hence is a measure of the steady state *peaking* for the species in question.


... to the problem
==================
Making the legacy code used for deriving peaking factors meets with two main 
problems addressed in this project: 

* *packaging* the modules for distribution,
* and *documenting* the code so that it is useful for the uninitiated

Packaging
---------
Packaging is performed using pythons :py:mod:`distutils` module [distutils]_, 
as introduced in the course.

Documenting
-----------
The documentation consists of an automatically generated part, based on the 
``docstrings`` from the python module, and of a more comprehensive text,
written by hand.

Both are implemented using the Sphinx [Sphinx] framework, as introduced in the
course.

