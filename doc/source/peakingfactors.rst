peakingfactors Package
======================

:mod:`peakingfactors` Package
-----------------------------

.. automodule:: peakingfactors.__init__
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`dataops` Module
---------------------

.. automodule:: peakingfactors.dataops
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`peaking` Module
---------------------

.. automodule:: peakingfactors.peaking
    :members:
    :undoc-members:
    :show-inheritance:

