#########################
Appendix B: Original plan
#########################

*********************************
Python and HPC 2012: Project plan
*********************************
**Andreas Skyman** *(2012--02--02)*

Project description
===================

Packaging modules for distribution
""""""""""""""""""""""""""""""""""
The project will aim to take code that I have written previously, update it as
neccesary, according to the suggestions from the course, and package it using
*distutils*.  A rudimentary documentation will also be provided, based on
Sphinx.

Stepping stones
"""""""""""""""
The code in question subsists of a few python scripts that use numpy to analyse
dataseries. The files are currently not organised properly as modules, so this
would be the first step.

The second step will consist of updating the standards used in the code, to
those sugested in the course (e.g. name space separation, propert doc-strings,
GPL-license headers etc.).

The third, and most importat step, will be to create a distributable module
using distutils.

Finally, a documentaton will be written and included in the package, using the
*Sphinx* standard.

Learning outcomes
"""""""""""""""""
The anticipated outcome is that my modules will, at the end of this project, be
in a comprehensible format suitable for sharing them with co-workers. More
importantly, however, I anticipate that I will learn some of the valuable
practicalities involved in writing a maintainable and distributable code,
including documentation.

Time plan
=========

=============== =========   ====
Activity        Time: (h)   (wd)
=============== =========   ====
Planning:             2 h     
Writing plan:         2 h     
Implementaiton:      24 h    
Writing report:      12 h    
total:               40 h   5 wd
=============== =========   ====

Additional comments
"""""""""""""""""""
A possible extention of the project would be to write tests, such as *unit
tests*. This will be done only time permittting.

