##########
References
##########

.. [c3se] http://www.c3se.chalmers.se/index.php/Python_and_High_Performance_Computing#Project
.. [GENE] http://gene.rzg.mpg.de/
.. [distutils] http://docs.python.org/distutils/
.. [Sphinx] http://sphinx.pocoo.org/
.. [apidoc] http://sphinx.pocoo.org/invocation.html#invocation-of-sphinx-apidoc
.. [rst] http://sphinx.pocoo.org/rest.html
.. [matplotlib] http://matplotlib.sourceforge.net/faq/usage_faq.html#coding-styles
.. [git] https://gitorious.org/plasma-transport-analysis/impurity-peaking-factors
.. [setuptools] https://launchpad.net/ubuntu/natty/+package/python-setuptools
.. [PyPI] http://pypi.python.org/pypi?%3Aaction=list_classifiers
.. [copyleft] http://www.gnu.org/copyleft/
.. [GPL] http://www.gnu.org/licenses/gpl-3.0.html
.. [unittest] http://docs.python.org/library/unittest.html
.. [doctest] http://docs.python.org/library/unittest.html#module-unittest
