"""peakingfactors: Module for calculating impurity peaking factors 

Contains routines for manipulating *GENE* *nrg*-data, and calculating
peaking factors and the like based on this data.

:author: Andreas Skyman
:email: andreas.skyman@chalmers.se
:affiliation: Department of Earth and Space Sciences, Chalmers University of Technology, Sweden
:license: GPLv3
"""

import dataops
import peaking
