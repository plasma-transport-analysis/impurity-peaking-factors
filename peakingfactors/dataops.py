""" A module for reading and manipulating *nrg*-data from *GENE* simulations that 
has been reordered using the ``reorder2.py`` script.

Contains functions for reading data from file, and for manipulating the obtained
arrays for ease of futher processing, dealing mainly with quasi-linear data.

All functions work closely with :py:mod:`numpy` and hence *array* should be taken
to mean :py:class:`numpy.array`.
"""

#   dataops.py -- module for reading and manipulating nrg-data that has 
#   been reordered by the reordered2.py script.
#   Copyright (C) 2011  Andreas Skyman (skymandr@chalmers.se)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import os
import peaking as p


def readGammas(pth='Z/ITG/', L=8, M=31, N=1, toread=7, thespec=-1, nspec=3):
    r""" Function for batch reading fluxes (:math:`\Gamma`) and other data from 
    *nrg* data files that have been reordered using the reorder2.py script. 
    Expects files to have the extention ``.mat``

    Prints header from data files to ease debugging, since the order of the 
    data depends on what parameter scans have been performed.

    :param string pth: path to directory from where to read files
    :param integer L:  number of entries in first data dimension
    :param integer M:  number of entries in second data dimension
    :param integer N:  number of entries in third data dimension; by default `N=1`
    :param integer toread:  number signifying which data entry is of interest; by default `toread=7` (:math:`\Gamma_{es}`)
    :param integer thespec: which species to read; by default `thespec=-1` (the last)
    :param integer nspec:   the number of species in the file; by default `nspec=3`

    :returns:   array of data extracted from files in the ``pth`` directory, corresponding to the ``toread`` parameter
    :rtype:     two dimensional array of size :math:`L \times M` if :math:`N=1`, three-dimensional array of size :math:`L \times M \times N` otherwise

    """

    with open(pth + '/scan.log') as f:
        header = f.readline()
    if N == 1:
        Gs = np.zeros((L, M))
        for l in xrange(L):
            for m in xrange(M):
                file_n = l*M + m + 1 
                filename = "nrg_" + str(file_n).zfill(4) + ".mat"
                print "reading: " + filename 
                try:
                    temp = np.loadtxt(os.path.join(pth, filename))
                    Gs[l, m] = temp[thespec, toread - 1] \
                               / np.abs(temp[-nspec, 2])
                except IOError:
                    print "File empty?"
    else:
        Gs = np.zeros((L, M, N))
        for l in xrange(L):
            for m in xrange(M):
                for n in xrange(N):
                    file_n = (l*M + m)*N + n + 1 
                    filename = "nrg_" + str(file_n).zfill(4) + ".mat"
                    print "reading: " + filename 
                    try:
                        temp = np.loadtxt(os.path.join(pth, filename))
                        Gs[l, m, n] = temp[thespec, toread - 1] \
                                      / np.abs(temp[-nspec, 2])
                    except IOError:
                        print "File empty?"
    print header
    return Gs

def findGammas(Gammas):
    r""" Takes an array of flux values (:math:`\Gamma`) of size 
    :math:`N_{Samples} \times N_{dns}` and searches every row for the index at 
    which the sign changes.

    If no sign change is found, the first positive value pair is returned.

    :param Gammas:  array with :math:`\Gamma` values 
    :type Gammas:   two-dimensional array of size :math:`N_{Samples} \times N_{dns}`

    :returns:   array of :math:`\Gamma`-pairs at which the sign change was found, and an array of indices correpsonding to the second values in the pairs.
    :rtype:     tuple consisting of a two-dimensional anda  one-dimensional array, of sizes :math:`N_{Samples} \times 2` and :math:`N_{Samples}` respectively.
    """
    
    rows = Gammas.shape[0]
    GammaPairs = np.zeros((rows, 2))
    Indices = np.zeros((rows))
    for r in xrange(rows):
        I = np.where(Gammas[r, :] > 0)[0]
        if I.size == 0:
            I = np.where(Gammas[r, :] <= 0)[0]
            i = I.max()
#            if i == shape(Gammas)[1] - 1:
#                i -= 1
        else:
            i = I.min()
            if i == 0:
                i += 1
        Indices[r] = i
        GammaPairs[r, :] = Gammas[r, i - 1:i + 1]

    return GammaPairs, Indices
    
def peakingList(Gammas, Indices, dns):
    r""" For pairs of :math:`\Gamma`:s corresponding to density gradient values 
    from ``dns`` at indices from ``Indices``, as returned from 
    :py:func:`findGammas`, computes zero-flux Peaking Factor (:math:`PF`) by 
    calling :py:func:`peaking.peakingfactor` with the specified values.

    :param Gammas: pairs of :math:`\Gamma` values as returned by :py:func:`findGammas`
    :type Gammas:  two-dimensional array of size :math:`N_{Samples} \times 2`

    :param Indices: array of indices corresponding to the entries in ``Gammas[:, 1]``, for use in selecting the approprite gradient values from ``dns``
    :type Indices:  one-dimensional array of size :math:`N_{Samples}`

    :param dns: array of density gradient values, at which :math:`\Gamma` values have been calculated
    :type dns:  one-dimensional array of size :math:`N_{dns}`
    
    :returns:   three arrays of Peaking Factors, Diffusion coefficients, and Convective Velocities 
    :rtype:     tuple consisting of three one-dimensional arrays, all of the size :math:`N_{Samples}`

    *(See help for functions in module* :py:mod:`peaking` *for more details!)*
    """

    rows = Gammas.shape[0]
    Ds, RVs, PFs = np.zeros(rows), np.zeros(rows), np.zeros(rows)
    for r in xrange(rows):
        i = np.int(Indices[r])
        Ds[r], RVs[r], S2 = p.linefit(dns[i - 1:i + 1], Gammas[r,:], np.ones(2))
        PFs[r] = p.peakingfactor(Ds[r], RVs[r])

    return PFs, Ds, RVs

