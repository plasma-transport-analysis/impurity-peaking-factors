""" A module for calculating peaking factors from *nrg*-data obtained from
that has been reordered by the ``reordered2.py`` script. 

Contains functions for calculating peaking factors and estimating their errors
using different methods, dealing with non-linear and quasi-linear data.

All functions work closely with :py:mod:`numpy` and hence *array* should be taken
to mean :py:class:`numpy.array`.
"""

#   peaking.py -- module for calculating peakin factors from nrg-data
#   that has been reordered by the reordered2.py script.
#   Copyright (C) 2011  Andreas Skyman (skymandr@chalmers.se)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from warnings import warn


def linefit(dn, Gamma, std):
    r""" Calculates the straight line fit to the supplied data points using a
    Bayesian scheme.

    The function takes data ``Gamma`` from sample points ``dn`` and computes the
    best fit of the function 

    .. math:: \Gamma_Z = -D_Z \nabla n_Z + n_Z V_Z \Leftrightarrow R \Gamma/n_Z = -D_Z R/L_{n_Z} + R V_Z, 

    where :math:`D_z` is the diffusion constant, and :math:`V_Z` the convective 
    velocity of the particle species considered. $R$ is the major radius of the 
    tokamak, :math:`n_Z` is the particle density of species :math:`Z`, and 
    :math:`L_{n_Z}` the characteristic length for the species gradient 
    (:math:`-\nabla n_Z/n_Z = 1/L_{n_Z}`).    

    The Bayesian method *(Sivia, 2006)* used actually uses the variance in its 
    computation, but for consistency with other functions in the module, the 
    function takes the standard deviation `std` as an argument.
    Please make sure that `std` has been duly compensated to account for the 
    strong correlation normaly present in the data, using the function 
    :py:func:`timestd`!
    
    :param dn:  logarithmic gradient values at which the fluxes (:math:`\Gamma`) have been calculated
    :type dn:   one-dimensional array of size :math:`N_{data}`
    :param Gamma:   flux values (:math:`\Gamma`) calculated at points ``dn``
    :type Gamma:    one-dimensional array of size :math:`N_{data}`
    :param std:     estimated uncertainties of the respective fluxes
    :type std:      one-dimensional array of size :math:`N_{data}`

    :returns:   the estimated straight line coefficients with the slope taking the role of the diffusion (:math:`D_Z`) and the offset taking the role of the convective velocity (:math:`R V_Z`), and the estiamted covariance matrix
    :rtype:     tuple of three values, the first two of type :py:class:`numpy.float`, and the third an array of size :math:`2 \times 2`
    """

    the_vars = std ** 2
    w_k = 2 / the_vars
    alpha = np.sum(w_k * dn ** 2)
    beta = np.sum(w_k)
    gamma = np.sum(w_k * dn)
    p = np.sum(w_k * dn * Gamma)
    q = np.sum(w_k * Gamma)

    D_z  = (beta * p - gamma * q) / (alpha * beta - gamma ** 2)
    RV_z = (alpha * q - gamma * p) / (alpha * beta - gamma ** 2)

    sigma2 = (2 * np.array([[beta, -gamma], [-gamma, alpha]]) /
             (alpha * beta - gamma ** 2))

    return D_z, RV_z, sigma2

def peakingfactor(D_z, RV_z):
    r""" Calculates the zero-flux Peaking Factor :math:`PF_0` from supplied diffusion
    constant :math:`D_>` and convective velocity :math:`R V_Z`.
 
    The zero-flux Peaking Factor is defined :math:`PF_0 = -R V_Z/D_Z`. 

    :param float D_z:   diffusion coefficient (:math:`D_Z`)  
    :param float RV_z:  convective velocity (:math:`R V_Z`)

    :returns float:   the peaking factor (:math:`PF_0`) calcualted as described above.
    """

    PF0 = -RV_z / D_z

    return PF0

def peakingerror(C):
    r""" Calculates errorbars for sample based on the covariance matrix 
    :math:`\sigma^2`, for the zero-flux Peaking Factor :math:`PF_0`.

    Errorbars are calculated in two ways: with and without taking the covariance
    into account *(Sivia, 2006)*. Both values are returned.

    :param tuple C: a tuple such as that supplied by :py:func:`linefit`. 

    :returns tuple: tuple of estimated standard errors, the first without taking the covariance into account.
    """

    D_z, RV_z, sigma2 = C[0], C[1], C[2]
    PF0 = peakingfactor(D_z, RV_z)
    S2  = PF0 ** 2 * (sigma2[0, 0] / D_z ** 2 + sigma2[1, 1] / RV_z ** 2)
    S2c = (PF0 ** 2 * (sigma2[0, 0] / D_z ** 2 + sigma2[1, 1] / RV_z ** 2 -
          2 * sigma2[0, 1] / (D_z * RV_z)))
    S  = np.sqrt(S2)
    Sc = np.sqrt(S2c)

    return (S, Sc)

def get_data(filename, toread=7, toffs=0.0, spec=-1, return_time=False):
    r""" Function for getting nonlinear *nrg* time series data from file.

    :param string filename: file from which to read the data
    :param int toread:  index of data to read (by default reads index ``7``, corresponding to :math:`\Gamma_{es}`)
    :param float toffs:     time offset, only returns data for `t >= toffs`` (defaults to ``0.0``)
    :param int spec:    which species to read (by default reads last index, which is usually the impurity)
    :param  bool return_time:   if ``True``, return extracted times, along with data

    :returns:   either returns array of extracted data, or the extracted data and an array with the extracted time stamps
    :rtype:     one-dimensional array or tuple of two one-dimensional arrays of the same size, depending on ``return_time``
    """

    temp = np.loadtxt(filename)
    nspec = temp[:, 0].max()
    times = temp[nspec + spec:spec:nspec, 1]
    data  = temp[nspec + spec:spec:nspec, toread - 1]
    noffs = np.min(np.flatnonzero(times >= toffs))

    if return_time:
        return data[noffs:], times[noffs:]
    else:
        return data[noffs:]

def timeavg(filename, toread=7, toffs=0.0, spec=-1):
    r""" Returns time average of *nrg* data.

    For data in the file whose name is supplied, the function returns data 
    from column ``toread`` in the dataset (array column ``toread - 1``). 
    Times for the data points are stored in column 2 (array column ``1``). 
    This is done for the last particle species (species ``nspec``), which is 
    found by looking at the maximum value of the first data column (array 
    column ``0``).

    :param string filename: file from which to read the data
    :param in toread:   index of data to read (by default reads index ``7``, corresponding to :math:`\Gamma_{es}`)
    :param float toffs:     time offset, only returns data for `t >= toffs`` (defaults to ``0.0``)
    :param int spec:    which species to read (by default reads last index, which is usually the impurity)
    
    :returns float: the average of the data set, under the specified conditions
    """

    data, times = get_data(filename, toread, toffs, spec, True)
    t0, tN = times[0], times[-1]
    dt = (times[2:] - times[0: -2]) * 0.5
    avgdata = np.sum(data[1: -1] * dt) * 2.0 / (tN - t0)

    return avgdata

def timestd(filename, toread=7, toffs=0.0, method='decay', spec=-1, kmax=666,
            simple=False, blen=1000, test=True):
    r""" Returns standard deviation of a data set, compensated to account for
    correlations in the data.

    For data in file ``filename``, the function first estimates the raw standard
    error :math:`\sigma_{raw}`. The effective samplerate is then estimated as 
    :math:`s/N`, where :math:`N` is the number of data points and :math:`s` is 
    correction factor. The correction :math:`s` is computed using either using 
    block averaging or by  looking at the decay of the auto-correlation function 
    -- the method is chosen by setting ``method`` to either ``'block'`` or 
    ``'decay'``. The compensated standard deviation is then computed as:
    
    .. math:: \sigma_{eff} = \sigma_{raw}\sqrt{s/N}.

    The computations are performed on the data from column ``toread`` in 
    the dataset (array column ``toread - 1``). Times for the data points 
    are stored in column 2 (array column ``1``). Analysis is performed for the last
    particle species (species ``spec``), which is found by looking at the 
    maximum value of the first data column (array column ``0``). 

    :param string filename: file from which to read the data
    :param int toread:  index of data to read (by default reads index ``7``, corresponding to :math:`\Gamma_{es}`)
    :param float toffs:     time offset, only returns data for `t >= toffs`` (defaults to ``0.0``)
    :param string method:   string specifying method, can be either ``'decay'`` (for :py:func:`decayavg`) or ``'block'`` (for :py:func:`blockavg`), default is ``'decay'``
    :param int spec:    which species to read (by default reads last index, which is usually the impurity)
    :param int kmax:    max correlation length, as defined in :py:func:`decayavg` (default is ``666`` for reasons explained in :py:func:`decayavg`)
    :param bool simple: whther to use simplifyed decay estimate, as defined in :py:func:`decayavg` (default is ``False``)
    :param int blen:    block length, as defined in :py:func:`blockavg` (Default is ``True``
    :param bool test:   whether to test block sizes, as defined in py:func:`blockavg` (Default is ``1000``

    :returns tuple: compensated standard deviation, the total number of samples N, and the sample length s

    :raises OptionError: if ``method`` is neither ``'decay'`` nor ``'block'``
    """

    temp = np.loadtxt(filename)
    nspec = temp[:, 0].max()
    times = temp[nspec + spec:spec:nspec, 1]
    data  = temp[nspec + spec:spec:nspec, toread - 1]
    noffs = np.min(np.flatnonzero(times >= toffs))
    t0, tN = times[noffs], times[-1]
    dt = times[noffs + 1:-1] - times[noffs:-2]    
    N = dt.size

    if method == 'block':
        s = blockavg(times[noffs + 1:-1], data[noffs + 1:-1], test=test, blen=blen)
    elif method == 'decay':
        s = decayavg(times[noffs + 1:-1], data[noffs + 1:-1], kmax=kmax, simple=simple)
    else:
        try:
            raise OptionError(method)
        except:
            print 'Unknown correlation method: ' + method + " Using s = 1."    
            s = 1.0

    raw_std = np.sqrt(np.var(data[noffs + 1:-1]))
    std = raw_std*np.sqrt(s/N)

    return std, N, s

def blockavg(t, d, test=False, blen=1000):
    r""" Estimates correlation in data set ``d,`` collected at sample times ``t``,
    through the method of 'block averaging'.

    :param t:   array of sample times, such those supplid by :py:func:`get_data` with ``return_time = true``
    :type t:    array of size :math:`N_t`
    :param d:   array of data samples, such those supplied by :py:func:`get_data`
    :type d:    array of size :math:`N_t`
    :param bool test:   parameter used for deugging; if ``test==True`` prints various data for different values of `blen` (default is ``False``)
    :param int blen:    length of block used in the block averaging (default is ``1000``)

    :returns float: estimated correlation

    :raises OptionError: if ``test`` neither ``'yes'`` nor ``'no'``
    """

    dt = (t[2:] - t[: -2]) * 0.5

    if test:
        for B in (10, 20, 50, 100, 200, 500, 1000, 2000):
            N = B*(np.size(d[1:-1], 0)/B)
            data = d[1:N+1]*dt[1:N+1]/dt[1:N+1].mean()
            F = np.mean(np.reshape(data, (N/B, B)), 1)
            s = B*F.var() / data.var()
            print s, F.var(), data.var(), B, N/B, F.shape, data.shape
    else:
        N = blen*(np.size(d[1:-1], 0)/blen)
        data = d[1:N+1]*dt[1:N+1]/dt[1:N+1].mean()
        F = np.mean(np.reshape(data, (N/blen, blen)), 1)
        s = blen*F.var()/data.var()

    return s

def decayavg(t, d, kmax=666, simple=True):
    r""" Estimates correlation in data set ``d``, collected at sample times 
    ``t``, by looking at the decay of the correlation function.
    
    :param t:   array of sample times, such those supplid by :py:func:`get_data` with ``return_time = true``
    :type t:    array of size :math:`N_t`
    :param d:   array of data samples, such those supplied by :py:func:`get_data`
    :type d:    array of size :math:`N_t`
    :param int kmax:    For the call to :py:func:`autocorr`, ``kmax`` should be chosen so that :math:`k_{max} \ll N_{data}``, but also so that :math:`\Phi(k)` drops below :math:`\exp(-2)` (:math:`\exp(-2) \approx 0.135`) for :math:`k \approx k_{max}`. To acertain this, the parameter ``plotit`` can be set to ``'yes'``. By default ``kmax = 666`` (I was listening to Iron Maiden's '7th Son of a 7th Son' as I wrote this, but it also suited my test data set...). 
    :param bool simple: switch for turning on simplified estimation of effective sample size, using the one step correlation and assuming exponential decay (default is ``True``; this will almost certainly under-estimate the effective sample size!)

    :returns float: estimated correlation which is interpreted as a 'sample length' :math:`s`, such that if :math:`N` is the number of data points, :math:`N/s` is the effective number of samples.
    """

    s = 1.0
    s = 1.0
    dt = (t[2:] - t[: -2]) * 0.5
    data = d[1: -1] * dt / dt.mean()
    Phi = autocorr(data, kmax, plotit = 'no')

    if simple:
        lmbda = -np.log(Phi[1])
        s = 2 / lmbda
    else:
        maxind = np.argmin(Phi > 0.0)
        if maxind == 0:
            maxind = -1
        elif maxind > np.exp(-2) * data.size:
            maxind = np.int(np.exp(-2) * data.size)
        #s = 2*np.sum(np.where(Phi > np.exp(-2), Phi, 0)) - Phi[0] # This is a bit of a HACK
        s = 2 * np.sum(Phi[1:maxind]) + 1
        #print "    exp(-2) samples: {0}".format(data.size /(1.0 * np.argmin(Phi > np.exp(-2))))

    if s < 0:
        print s, Phi
        warn("decayavg returned a negative number! Returning 1.0!")
        s = 1.0

    return s

def autocorr(d, kmax=100, plotit='no', usenumpynitwit=True):
    r""" Calculates the auto-correlation function for data set ``d``.

    :param d:   array of data samples, such those supplied by :py:func:`get_data`
    :type d:    array of size :math:`N_t`
    :parameter int kmax:    The auto-correlation (:math:`\Phi(k)`) is calculated up to a distance :math:`k_{max}`, including a distance of :math:`0`, meaning that :math:`\Phi` has :math:`k_{max} + 1` entires. By default ``kmax = 100``, though this may be too short for many purposes. 
    :param string plotit:   mainly for debugging, if ``plotit == 'yes'``, will invoke :py:mod:`matplotlib` and plot :math:`\Phi(k)`
    :param bool usenumpynitwit: toggles using numpy's builtin correlate, rather than doing something contrived...

    :returns:   evaluated auto-correlation function (:math:`\Phi(k)`)
    :rtype:     one-dimensional array of size ``kmax + 1``

    *(This could more appropriately be accomplished by using* :py:mod:`scipy.signal` *!)*
    """

    if usenumpynitwit:
        n = d.size
        Phi = (np.correlate(d, d, 'full')[-n:] / (n - np.arange(n)) 
              - d.mean() ** 2) / d.var()
    else:
        Phi = np.zeros(kmax + 1)

        for k in xrange(kmax + 1):
            Phi[k] = ( np.mean(d[k:-1]*d[0:-(k + 1)]) - np.mean(d[0:-(k + 1)])**2 )/ \
                     ( np.mean(d[0:-(k + 1)]**2) - np.mean(d[0:-(k + 1)])**2 )
    if plotit == 'yes':
        import matplotlib.pyplot as plt
        plt.plot(np.arange(kmax + 1), Phi)
        plt.plot(np.array([0, kmax + 1]), np.array([1, 1])*np.exp(-2))
        plt.grid('on')

    return Phi

class OptionError(Exception):
    r""" Error raised if invalid option is passed to a function.
    """

    def __init__(self, value):
         self.value = value

    def __str__(self):
         return repr(self.value)

