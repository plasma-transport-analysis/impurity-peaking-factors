#! /usr/bin/env python

#   setup.py -- distutils script for setting up and packaging the
#   "Impurity Peaking Factors" module (peakingfactors).
#   Copyright (C) 2011  Andreas Skyman (skymandr@chalmers.se)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

from distutils.core import setup

setup(name="PeakingFactors",
      version="0.9",
      description="Module for analysing peaking factors from GENE nrg data.",
      author="Andreas Skyman",
      author_email="skymandr@chalmers.se",
      url="https://gitorious.org/plasma-transport-analysis/",
      license='GPLv3',
      packages=['peakingfactors'],
      scripts=['tools/reorder2.py'],
      requires=['numpy', 'matplotlib'],
      classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Science/Research',
        'Intended Audience :: Developers',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: GNU General Public License (GPL)',
        'Natural Language :: English',
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: Microsoft :: Windows',
        'Operating System :: POSIX',
        'Programming Language :: Python 2',
        'Topic :: Scientific/Engineering :: Physics',
        ],
      )
      
