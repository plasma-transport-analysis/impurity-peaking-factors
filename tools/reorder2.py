#! /usr/bin/env python
#   reorder2.py -- command line script  for reordering nrg-data files to a
#   structure that can be easily imported using numpy standard tools.
#   Copyright (C) 2011  Andreas Skyman (skymandr@chalmers.se)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

import glob

n_spec = 3
in_ = "nrg"
filelist = glob.glob(in_ + '*')

for filename in filelist:
    if not filename.endswith('.mat'):
        with open(filename, 'rb') as f_in:
            with open(filename + '.mat', 'wb') as f_ut:
                t = f_in.readline()
                while t:
                    t = float(t)
                    for n in range(n_spec):
                        s = str(n + 1) + " " + str(t) + " " + f_in.readline()
                        f_ut.write(s)
                    t = f_in.readline()


